<?php
/**
 * Template Name: Vragenlijst
 *
 * @package wegwijsdag
 */

get_header(); ?>

<script>

	jQuery( document ).ready( function( $ ) {

			// Pas volgende blok tonen na 2 keuzes bij profielen (bij route 'nee/twijfel')
	//     $('.two_choise input[type=checkbox]').on('change', function() {
	//         if($('.two_choise input[type=checkbox]:checked').length < 2) {
	//            $('#field_3_10').css("display","none");
	//         }else{
	//             $('#field_3_10').css("display","block");
	//         }

	//     });

		// pa volgende pagina-knop tonen na 2 keuzes bij interesses
	    $('.interest_choise input[type=checkbox]').on('change', function() {
	        if($('.interest_choise input[type=checkbox]:checked').length < 2) {
	           $('.gform_page_footer input[type="button"]').css("display","none");
	        }else{
	            $('.gform_page_footer input[type="button"]').css("display","inline-block");
	        }

	    });
	} );
</script>
	<div class="contentTop">
		<div class="subMenu">
			<?php get_sidebar('Submenu'); ?>
		</div>
	</div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->
	<div class="vragenlijstSection">
		<?php  echo do_shortcode( '[gravityform id="3" title="false" description="false" ajax="true"]' ); ?>
	<div style="clear: both;"></div>
</div>
<div class="bottomImage">
	<img src="https://www.wegwijsdaghbo.nl/wp-content/uploads/2017/10/artiest.png" />
</div>
<?php
get_footer();
