<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * Template Name: Profielen
 * @package wegwijsdag
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

			<div class="profileblock">
				<ul>
					<li><a class="blue" href="<?php echo esc_url( site_url( '/' ) ); ?>profielen/cultuur-maatschappij">Cultuur &<br/> Maatschappij</a></li>
					<li><a class="yellow" href="<?php echo esc_url( site_url( '/' ) ); ?>profielen/economie-maatschappij">Economie &<br/> Maatschappij</a></li>
					<li><a class="green" href="<?php echo esc_url( site_url( '/' ) ); ?>profielen/natuur-gezondheid">Natuur &<br/> Gezondheid</a></li>
					<li><a class="pink" href="<?php echo esc_url( site_url( '/' ) ); ?>profielen/natuur-techniek">Natuur &<br/> Techniek</a></li>
				</ul>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
