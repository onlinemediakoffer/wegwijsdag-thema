<?php

//[foobar]
function wwd_workshop( $atts ){
	wp_enqueue_script( 'mixitup', get_stylesheet_directory_uri() . '/js/mixitup.min.js', array() );
	wp_enqueue_script( 'mixitup-multifilter', get_stylesheet_directory_uri() . '/js/mixitup-multifilter.min.js', array() );
	wp_enqueue_script( 'workshop-kiezer', get_stylesheet_directory_uri() . '/js/workshop-kiezer.js', array( 'jquery' ) );



	wp_enqueue_style( 'wegwijsdag-workshop-style', get_template_directory_uri() . '/layouts/workshop-min.css' );


	?>

	<div id="keuze-sidebar">
		<fieldset data-filter-group class="checkbox-group">
			<section class="keuzeSector">
					<label class="checkbox-group-label"><h3>Selecteer jouw sector</h3></label>

					<div class="checkbox">
						<input class="check" id="life-science" type="checkbox" name="type-sector" value=".life-science-natuur-milieu">
						<label class="checkbox-label" for="life-science">Life Science, Natuur & Milieu</label>
					</div>

					<div class="checkbox">
						<input class="check" id="techniek" type="checkbox" name="type-sector" value=".techniek">
						<label class="checkbox-label" for="techniek">Techniek</label>
					</div>

					<div class="checkbox">
						<input class="check" id="zorg-welzijn-onderwijs" type="checkbox" name="type-sector" value=".zorg-welzijn-onderwijs">
						<label class="checkbox-label" for="zorg-welzijn-onderwijs">Zorg, Welzijn & Onderwijs</label>
					</div>

					<div class="checkbox">
						<input class="check" id="communicatie-cultuur-vrije-tijd" type="checkbox" name="type-sector" value=".communicatie-cultuur-vrije-tijd">
						<label class="checkbox-label" for="communicatie-cultuur-vrije-tijd">Communicatie, Cultuur & Vrije Tijd</label>
					</div>

					<div class="checkbox">
						<input class="check" id="economie-management-bestuur" type="checkbox" name="type-sector" value=".economie-management-bestuur">
						<label class="checkbox-label" for="economie-management-bestuur">Economie, Management & Bestuur</label>
					</div>
			</section>
		</fieldset>
	</div>

	<section id="workshops">
		<h2>Kies drie workshops</h2>

		<form id="workshopSelect" class="container" action="<?php echo site_url(); ?>/bijna-klaar/">

			<div class="mix life-science-natuur-milieu">
				<input id="voedzame-smoothie" type="checkbox" name="type-workshop" value="Maak je eigen voedzame smoothie" class="show workshop-checkbox doener">
				<label for="voedzame-smoothie" class="show workshop-block doener">
					<div class="skew">
						<h4>Maak je eigen voedzame smoothie</h4>
						<p>Bedenk een smoothie om te verkopen op de Oryente Food market. Zorg dat hij de juiste voedingsstoffen heeft, maar ook lekker is. En natuurlijk denk je ook na over een milieuvriendelijke verpakking.</p>
					</div>
				</label>
			</div>

			<div class="mix life-science-natuur-milieu">
				<input id="ideale-dierenverblijf" type="checkbox" name="type-workshop" value="Schets het ideale dierenverblijf" class="show workshop-checkbox creatief">
				<label for="ideale-dierenverblijf" class="show workshop-block creatief">
					<div class="skew">
						<h4>Schets het ideale dierenverblijf</h4>
						<p>Op het eiland Oryente leven verschillende dieren. Ontwerp jij de ideale verblijfruimte voor deze dieren? Denk na over voldoende beweegruimte, groepsgroottes en de toegang tot voedsel.  </p>
					</div>
				</label>
			</div>

			<div class="mix techniek">
				<input id="ontwerp-game" type="checkbox" name="type-workshop" value="Hoe ontwerp je een game?" class="show workshop-checkbox techniek">
				<label for="ontwerp-game" class="show workshop-block techniek">
					<div class="skew">
						<h4>Hoe ontwerp je een game?</h4>
						<p>Je kent het vast – je bent met je ouders op vakantie en de verveling slaat toe. Bedenk daarom een game die jongeren tijdens hun vakantie kunnen spelen. Daarna ga je aan de slag met het maken van een eigen game!</p>
					</div>
				</label>
			</div>

			<div class="mix techniek">
				<input id="slimme-apparaten" type="checkbox" name="type-workshop" value="Ontwikkel slimme apparaten" class="show workshop-checkbox">
				<label for="slimme-apparaten" class="show workshop-block">
					<div class="skew">
						<h4>Ontwikkel slimme apparaten</h4>
						<p>Welke handige, briljante, bizarre en unieke technische snufjes ontwikkel jij voor de mensen op Oryente? Bedenk slimme robots vol elektronische snufjes. Zorg met een micro:bit en elektronica dat het luxe cruiseschip niet meedeint op de golven, zodat de gasten van Oryente niet zeeziek worden.</p>
					</div>
				</label>
			</div>

			<div class="mix zorg-welzijn-onderwijs">
				<input id="nieuwe-onderwijs" type="checkbox" name="type-workshop" value="Het nieuwe onderwijs" class="show workshop-checkbox sociaal">
				<label for="nieuwe-onderwijs" class="show workshop-block sociaal">
					<div class="skew">
						<h4>Het nieuwe onderwijs</h4>
						<p>Oryente wil graag een plek bieden waar iedereen, jong of oud, zich kan ontwikkelen. Maar hoe ziet de ideale school eruit? Beschrijf jouw ideale les of docent en debatteer met elkaar over hoe het onderwijs eruit moet zien.</p>
					</div>
				</label>
			</div>

			<div class="mix zorg-welzijn-onderwijs">
				<input id="gezondheids-centrum" type="checkbox" name="type-workshop" value="Het gezondheidscentrum van Oryente" class="show workshop-checkbox">
				<label for="gezondheids-centrum" class="show workshop-block">
					<div class="skew">
						<h4>Het gezondheidscentrum van Oryente</h4>
						<p>Hoe voelt het wanneer je afhankelijk bent en zorg nodig hebt? En wat heb je dan nodig? Ervaar hoe je mensen kunt begeleiden en denk na over hoe de ideale plek eruit ziet waar iedereen die dat nodig heeft verzorgd of geholpen kan worden.</p>
					</div>
				</label>
			</div>


			<div class="mix communicatie-cultuur-vrije-tijd">
				<input id="organiseer-festival" type="checkbox" name="type-workshop" value="Organiseer het Oryente Festival" class="show workshop-checkbox doener creatief">
				<label for="organiseer-festival" class="show workshop-block doener creatief">
					<div class="skew">
						<h4>Organiseer het Oryente Festival</h4>
						<p>Op Oryente wordt een groot meerdaags festival georganiseerd. Door middel van een brainstorm bedenk jij de programma onderdelen en hoe het festival georganiseerd moet worden. Zorg jij ervoor dat het ‘t tofste festival ooit wordt?</p>
					</div>
				</label>
			</div>

			<div class="mix communicatie-cultuur-vrije-tijd">
				<input id="communicatie-marketingplan" type="checkbox" name="type-workshop" value="Communicatie- en marketingplan Oryente Festival" class="show workshop-checkbox doener creatief">
				<label for="communicatie-marketingplan" class="show workshop-block doener creatief">
					<div class="skew">
						<h4>Communicatie- en marketingplan Oryente Festival</h4>
						<p>Hoe laten we de wereld weten dat er een fantastisch festival plaatsvindt? Kies een doelgroep en denk na over het communicatie- en marketingplan. Ontwikkel ook een campagnebeeld en een boodschap om de doelgroep naar Oryente te laten komen.</p>
					</div>
				</label>
			</div>

			<div class="mix economie-management-bestuur">
				<input id="ontwerp-vakantiepark" type="checkbox" name="type-workshop" value="Ontwerp een vakantiepark" class="show workshop-checkbox doener creatief">
				<label for="ontwerp-vakantiepark" class="show workshop-block doener creatief">
					<div class="skew">
						<h4>Ontwerp een vakantiepark</h4>
						<p>Op Oryente wil de burgemeester een nieuw vakantiepark bouwen op de plek waar nu een vuilnisbelt staat. Maar hoe moeten we dit doen? Je maakt hiervoor een plan waarbij je natuurlijk ook nadenkt over werkgelegenheid, financiën en duurzaamheid.</p>
					</div>
				</label>
			</div>

			<div class="mix economie-management-bestuur">
				<input id="ondernemersplan-oryente" type="checkbox" name="type-workshop" value="Het ondernemersplan voor attractiepark Oryente" class="show workshop-checkbox doener creatief">
				<label for="ondernemersplan-oryente" class="show workshop-block doener creatief">
					<div class="skew">
						<h4>Het ondernemersplan voor attractiepark Oryente</h4>
						<p>Help het ondernemersplan voor het nieuwe attractiepark op te stellen zodat aan alles gedacht wordt om er een goedlopend bedrijf van te maken. Denk mee over hoe we het park kunnen financieren, welke mensen we nodig hebben en hoe we reclame gaan maken.</p>
					</div>
				</label>
			</div>

			<div id="workshop_submitbar">
				<button name="submit" type="submit">Stap 2: gegevens invullen</button>
			</div>
		</form>
	</section>


	<?php
}
add_shortcode( 'wwd_workshop_select', 'wwd_Workshop' );
?>