<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wegwijsdag
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="footerFirstRow">
			<div class="footerLogo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/wegwijsdag_2020_logo_white.png" /></a>
			</div>
		
			<div class="logosScholen">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos-scholen.svg" />
			</div>
		</div>
		<div class="footerSecondRow">
			<div class="footerMenu">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'footer-menu',
					'menu_id'        => 'footer-menu',
				) );
			?>
			</div>
			<div class="discMenu">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'disclaimer-menu',
					'menu_id'        => 'disclaimer-menu',
				) );
			?>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
