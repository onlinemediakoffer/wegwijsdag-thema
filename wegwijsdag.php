<?php
/**
 * Template Name: Wegwijsdag
 *
 * @package wegwijsdag
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<div class="HpBankier"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/arts.png"></div>
		<div class="HpBankier"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/artiest.png"></div>
		<main id="main" class="site-main" role="main">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
		<div class="contentSection">
			<div class="row">
				<div class="quote">
					<div class="quoteContent">
						<h3>“Ik kies voor mensenwerk!”</h3><br/>
						"De workshop die ik koos startte met een opdracht. We moesten zonder te praten onszelf in een rij zetten van hoog naar laag. Dat was leuk en ging vooral over communiceren. Daarna hebben we gekeken naar hoe het is om met mensen te werken die hulp nodig hebben. De docent liet met een voorbeeld zien hoe gemakkelijk het is om eigenlijk iedereen wel een bepaald stempel te geven. Daardoor snapte ik dat het niet leuk is om een etiket opgeplakt te krijgen. Ik twijfel nog tussen E&M en C&M. Met techniek heb ik niks, dus dat wordt 'm niet. Ik heb nog geen idee wat voor studie ik ga kiezen. Misschien wel de studie van deze workshop: Sociaal Pedagogische Hulpverlening."<br/><Br/>
						<span>Ids Osinga<br/>
						Havo 3<br/>
						csg Bogerman, Balk<Br/></p></span>
					</div>
				</div>
				<div class="todo">
					<h2>Wat kun je verwachten op de Wegwijsdag?</h2>
					<p>
					Ben jij graag lekker actief bezig of houd je ervan over dingen na te denken? Heb je groene vingers, een hoofd vol creatieve ideeën of vind je techniek ontzettend leuk? Tijdens de Wegwijsdag ontdek je waar jouw talenten liggen. </p>
					<h3>Ontdek door te doen! </h3>
					<p>Stil zitten en luisteren naar een docent die jou vertelt wat je moet kiezen? Nope! Jij gaat zelf aan de slag tijdens twee workshops. Zo kun je meteen kennismaken met de profielen waar je uit kunt kiezen.  </p>
				</div>
			</div>
			<div class="row second">
				<div class="watDoen">
					<h2>Wat moet je doen?</h2><br/>
					<p>Nadat je de Profielkeuzecheck hebt ingevuld, kun jij je direct aanmelden voor de Wegwijsdag. Wij mixen en matchen je met de juiste twee workshops. 
					</p>
				</div>
				<div class="speelDeGame">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>profielkeuzecheck"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bigPlayButton3.png" /></a>
				</div>
			</div>
			<div class="row third">
				<div class="stepOne">
					<h5 class="green">Stap 1</h5>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>profielkeuzecheck">Vul de Profielkeuzecheck in</a></p>
				</div>
				<div class="stepTwo">
					<h5 class="pink">Stap 2</h5>
					<p>Meld je aan voor de Wegwijsdag</p>
				</div>
				<div class="stepThree">
					<h5 class="blue">Stap 3</h5>
					<p>Kom naar de Wegwijsdag, volg je workshops en ontdek welk profiel bij je past.</p>
				</div>
			</div>
			<div class="row four">
				<div class="intro">
					<h2>Sfeerimpressies vorige Wegwijsdag</h2>
					<p>Vorig jaar organiseerden we ook een Wegwijsdag. Maar liefst 1700 havo-leerlingen volgden leuke workshops en kregen een voorproefje van de studies en beroepen die mogelijk zijn met bepaalde profielen. Hieronder vind je een aantal foto's van deze dag om alvast een goed beeld te krijgen van wat jou te wachten staat.</p>
				</div>
				<div class="rigthImage">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bankier.png" />
				</div>
				<div class="fotoBlok">
					<div class="blockOne"></div>
					<div class="blockTwo"></div>
					<div class="blockThree"></div>
					<div class="blockFour"></div>
					<div class="blockFive"></div>
					<div class="blockSix"></div>
				</div>
				</div>
			</div>
		</div>
	</div><!-- #primary -->
	
<?php
get_footer();
